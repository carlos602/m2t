package m2_test.http;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by carlosyang on 16/3/30.
 */
public class TrustManagerTest {

    public static void main(String[] args) {

//
//        System.out.println(8*9);
//
//        DefaultHttpClient httpclient = new DefaultHttpClient();
//
//        SSLContext sslContext;
//        try {
//            sslContext = SSLContext.getInstance("SSL");
//
//            // set up a TrustManager that trusts everything
//            try {
//                sslContext.init(null,
//                        new TrustManager[] { new X509TrustManager() {
//                            public X509Certificate[] getAcceptedIssuers() {
//                                System.out.println("getAcceptedIssuers =============");
//                                return null;
//                            }
//
//                            public void checkClientTrusted(
//                                    X509Certificate[] certs, String authType) {
//                                System.out.println("checkClientTrusted =============");
//                            }
//
//                            public void checkServerTrusted(
//                                    X509Certificate[] certs, String authType) {
//                                System.out.println("checkServerTrusted =============");
//                            }
//                        } }, new SecureRandom());
//            } catch (KeyManagementException e) {
//            }
//            SSLSocketFactory ssf = new SSLSocketFactory(sslContext,SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
//            ClientConnectionManager ccm = httpclient.getConnectionManager();
//            SchemeRegistry sr = ccm.getSchemeRegistry();
//            sr.register(new Scheme("https", 443, ssf));
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        post("https://www.baidu.com/",null, new HashMap<>());


        System.out.println("===============================");

//        post("https://114.215.196.178/beehive-portal/api/triggers/"
//                ,null, new HashMap<>());
    }









    /**
     * 发送HttpPost请求
     *
     * @param strURL   服务地址
     * @param jsonBody json字符串,例如: "{ \"id\":\"12345\" }" ;其中属性名必须带双引号<br/>
     * @return 成功:返回json字符串<br/>
     */
    public static String post(String strURL, String jsonBody, Map<String, String> header) {
        System.out.println(strURL);
        System.out.println(jsonBody);
//		jsonBody = jsonBody.substring(1, jsonBody.length()-1);
        try {
            URL url = new URL(strURL);// 创建连接
            HttpURLConnection connection = (HttpURLConnection) url
                    .openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setUseCaches(false);
            connection.setInstanceFollowRedirects(true);
            connection.setRequestMethod("POST"); // 设置请求方式
//			connection.setRequestProperty("Accept", "application/json"); // 设置接收数据的格式
            connection.setConnectTimeout(20000);
            connection.setReadTimeout(300000);
//            connection.setRequestProperty("Content-Type", "text/xml; charset=GB2312");
//			connection.setRequestProperty("Content-Type", "application/json"); // 设置发送数据的格式
//			connection.setRequestProperty("Authorization", "Bearer k6X2Hfbr4rs2_Aii4GZTSf41G4txHFgKdGI2Bj2kGao");
//			connection.setRequestProperty("X-Kii-AppID", "89ba7c08");
//			connection.setRequestProperty("X-Kii-AppKey", "3fa8373acef12b424b335f9dbc18b367");
            for (String key : header.keySet()) {
                connection.setRequestProperty(key, header.get(key));
            }
            connection.connect();
            OutputStreamWriter out = new OutputStreamWriter(
                    connection.getOutputStream(), "UTF-8"); // utf-8编码
            out.append(jsonBody);
            out.flush();
            out.close();

            // 读取响应
            int responseCode = connection.getResponseCode();
            String responseMessage = connection.getResponseMessage();
            System.out.println("responseCode: " + responseCode);
            System.out.println("responseMessage: " + responseMessage);

            // 正常响应
            if(responseCode < 400){
                InputStream is = connection.getInputStream();
                BufferedReader read = new BufferedReader(new InputStreamReader(is, "UTF-8")); // utf-8编码

                StringBuffer buffer = new StringBuffer();

                String line = null;
                while((line = read.readLine()) != null) {
                    buffer.append(line);
                }

                is.close();
                read.close();

                String result = buffer.toString();
                System.out.println(result);
                return result;
            } else {
                // 错误响应
                InputStream is = connection.getErrorStream();

                StringBuffer buffer = new StringBuffer();
                byte[] temp = new byte[512];
                int readLen = 0;
                while((readLen = is.read(temp)) > 0) {
                    buffer.append(new String(temp, 0, readLen, "UTF-8"));
                }

                is.close();

                String result = buffer.toString();
                System.out.println(result);
                return result;
            }

        } catch (IOException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
        }
        return "error"; // 自定义错误信息
    }



}
