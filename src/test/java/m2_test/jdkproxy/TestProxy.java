package m2_test.jdkproxy;

public class TestProxy {

	public static void main(String[] args) {
		BookFacadeProxy proxy = new BookFacadeProxy();
		BookFacade bookProxy = (BookFacade) proxy.bind(new BookFacadeImpl());
		bookProxy.addBook();
		
//		BookFacadeProxy proxy = new BookFacadeProxy();
//		Object o  = proxy.bind(new BookFacadeImpl2());
//		System.out.println(o.getClass());
//		BookFacadeImpl2 bookProxy = (BookFacadeImpl2) proxy.bind(new BookFacadeImpl2());
//		bookProxy.addBook();
	}

}