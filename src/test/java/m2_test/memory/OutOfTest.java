package m2_test.memory;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by user on 16/5/16.
 */
public class OutOfTest {

	/**
	 * -XX:-UseGCOverheadLimit
	 * -Xmx100m
	 *
	 * @param args
	 */
	public static void main(String[] args) {

		List<String> list = new ArrayList<>();
		while(true){
			try {
				list.add("1");
			} catch (VirtualMachineError e) {
				e.printStackTrace();
				System.out.println(new Date().toLocaleString() + " list size: " + list.size());
				list = null;
				System.gc();
				break;
			}
		}

		System.out.println(new Date().toLocaleString() + " test :  = " + 8123*35);



	}
}
