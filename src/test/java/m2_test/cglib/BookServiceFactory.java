/**
 *开发单位：FESCO Adecco 
 *版权：FESCO Adecco
 *@author：xuy@rayootech.com
 *@since： JDK1.6
 *@version：1.0
 *@date：2015-3-19 上午10:17:26
 */

package m2_test.cglib;

import net.sf.cglib.proxy.Callback;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.NoOp;

/**
 * @ClassName: BookServiceFactory
 * @Description: TODO
 * @author xuy@rayootech.com
 * @date 2015-3-19 上午10:17:26
 * 
 */
public class BookServiceFactory {
	private static BookServiceBean service = new BookServiceBean();

	private BookServiceFactory() {
	}

	public static BookServiceBean getProxyInstanceByFilter(MyCglibProxy myProxy) {
		Enhancer en = new Enhancer();
		en.setSuperclass(BookServiceBean.class);
		en.setCallbacks(new Callback[] { myProxy, NoOp.INSTANCE });
		en.setCallbackFilter(new MyProxyFilter());
		return (BookServiceBean) en.create();
	}

	public static BookServiceBean getProxyInstance(MyCglibProxy myProxy) {
		Enhancer en = new Enhancer();
		// 进行代理
		en.setSuperclass(BookServiceBean.class);
		en.setCallback(myProxy);
		// 生成代理实例
		return (BookServiceBean) en.create();
	}
}
