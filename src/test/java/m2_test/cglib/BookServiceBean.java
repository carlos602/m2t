/**
 *开发单位：FESCO Adecco 
 *版权：FESCO Adecco
 *@author：xuy@rayootech.com
 *@since： JDK1.6
 *@version：1.0
 *@date：2015-3-19 上午10:17:10
 */

package m2_test.cglib;

/**
 * @ClassName: BookServiceBean
 * @Description: TODO
 * @author xuy@rayootech.com
 * @date 2015-3-19 上午10:17:10
 * 
 */
public class BookServiceBean {
	public void create() {
		System.out.println("create() is running !");
	}

	public void query() {
		System.out.println("query() is running !");
	}

	public void update() {
		System.out.println("update() is running !");
	}

	public void delete() {
		System.out.println("delete() is running !");
	}
}
