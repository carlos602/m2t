/**
 *开发单位：FESCO Adecco 
 *版权：FESCO Adecco
 *@author：xuy@rayootech.com
 *@since： JDK1.6
 *@version：1.0
 *@date：2015-3-19 下午02:49:50
 */

package m2_test.cglib;

/**
 * @ClassName: Client
 * @Description: TODO
 * @author xuy@rayootech.com
 * @date 2015-3-19 下午02:49:50
 * 
 */
public class Client {

	public static void main(String[] args) {
		
//		BookServiceBean service = BookServiceFactory.getProxyInstance(new MyCglibProxy("boss"));
//		service.create();
//		BookServiceBean service2 = BookServiceFactory.getProxyInstance(new MyCglibProxy("john"));
//		service2.create();
		
//		BookServiceBean service = BookServiceFactory.getProxyInstanceByFilter(new MyCglibProxy("jhon"));
//		service.create();
		
		BookServiceBean service2 = BookServiceFactory.getProxyInstanceByFilter(new MyCglibProxy("jhon"));
		service2.create();
		service2.query();
		
		
//		MyCglibProxy cglibProxy = new MyCglibProxy("jhon");
//		BookServiceBean sbean = (BookServiceBean) cglibProxy.getDaoBean(BookServiceBean.class);
//		sbean.create();
//		sbean.query();
		
		
	}
}
