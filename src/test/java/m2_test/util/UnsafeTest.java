package m2_test.util;

import java.lang.reflect.Field;

import sun.misc.Unsafe;

public class UnsafeTest {

	/**
	 * @author: yangxu
	 * @date: 2015-12-28上午10:33:00
	 * @param args
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalAccessException
	 * @throws IllegalArgumentException
	 * @throws Exception 
	 */
	public static void main(String[] args) throws NoSuchFieldException, SecurityException, IllegalArgumentException, IllegalAccessException, InstantiationException, Exception {
		Unsafe unsafe = getUnsafe();
		
		// This creates an instance of player class without any initialization
		Player p = (Player) unsafe.allocateInstance(Player.class);
		System.out.println(p.getAge()); // Print 0

		p.setAge(45); // Let's now set age 45 to un-initialized object
		System.out.println(p.getAge()); // Print 45
		
//		Player p = new Player();
		
//		Player mp = (Player)Class.forName("m2_test.util.Player").newInstance();
//		System.out.println(mp.getAge());
		
		
		
//		String password = new String("l00k@myHor$e");  
//		String fake = new String(password.replaceAll(".", "?"));  
//		System.out.println(password); // l00k@myHor$e  
//		System.out.println(fake); // ????????????  
//		getUnsafe().copyMemory(fake, 0L, null, toAddress(password), sizeOf(password));  
		   
//		System.out.println(password); // ????????????  
//		System.out.println(fake); // ????????????  
		
		
		
	}
	
	

	private static Unsafe getUnsafe() throws NoSuchFieldException, IllegalAccessException {
		Field f = Unsafe.class.getDeclaredField("theUnsafe"); // Internal
																// reference
		
		f.setAccessible(true);
		Unsafe unsafe = (Unsafe) f.get(null);
		return unsafe;
	}
}

class Player {
	private int age = 12;

	private Player() {
		this.age = 50;
	}

	public int getAge() {
		return this.age;
	}

	public void setAge(int age) {
		this.age = age;
	}
}
