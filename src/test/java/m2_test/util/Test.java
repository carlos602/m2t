package m2_test.util;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class Test {

	/**
	 * @author: yangxu
	 * @date: 2015-12-28上午10:33:00
	 * @param args
	 */
	public static void main(String[] args) {
		int a = (3 & 5);
		System.out.println(a);
		System.out.println(Integer.toBinaryString(3));
		System.out.println(Integer.toBinaryString(5));
	}

	private static void map_fanxing() {
		System.out.println(new Long(1).equals(new Integer(1)));
		Map<Long, Long> map = new HashMap<Long, Long>();
		map.put(new Long(1), new Long(1));// 这里的key是Long类型
		Integer key = new Integer(1);
		boolean result = map.containsKey(key);// 这里的key是Integer类型
		System.out.println(result);
		System.out.println(map.get(key.longValue()));
	}

	private static void t() {
		System.out.println(1 << 16);

		Map<String, Object> map = new HashMap<String, Object>(1);
		map.put("1", "a");
		map.put("2", "b");

		Map<String, Object> map2 = new ConcurrentHashMap<>();
		map2.put("1", "a");
		map2.put("2", "b");
	}

}
