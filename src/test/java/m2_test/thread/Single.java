package m2_test.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class Single{  
	private AtomicInteger atomic = new AtomicInteger(0);
	private int count = 0 ;
    public static Single sg = null;  
    private Single(){}  
      
    public  static synchronized Single getInstance(){  
        if(sg == null)  
        	sg = new Single();  
        return sg;  
    }  
    
    
    public synchronized String getValueAtomic(String ss){
    	atomic.set(0);
    	for (int i = 0; i < 100; i++) {
    		atomic.incrementAndGet();
		}
    	try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return atomic.get()+ss;
    }
    
    public synchronized String getValueInt(String ss){
    	count = 0;
    	for (int i = 0; i < 100; i++) {
    		++count;
		}
    	try {
			Thread.sleep(100);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	return count+ss;
    }
}  

