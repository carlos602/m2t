package m2_test.thread;

import java.util.Date;
import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ExecutorsTest {

	
	public static void main(String[] args) {
		
        for(int i = 0; i < 10;i++) {
//            System.out.println(Math.abs(new Random().nextInt())%10000);
        }
		
		ExecutorService pool = Executors.newFixedThreadPool(5);
		
		for (int i = 0; i < 10; i++) {


			
			pool.execute(new Runnable() {
				
				@Override
				public void run() {
					int sleepTime = Math.abs(new Random().nextInt())%10000;
					System.out.println(new Date().toLocaleString() + Thread.currentThread().getName() + " 线程正在执行 : " + sleepTime);
					try {
						Thread.sleep(sleepTime);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					
					System.out.println(new Date().toLocaleString() + Thread.currentThread().getName() + " 线程结束");
				}
			});
			
			
			
			
		}
		
		
		pool.shutdown();
	}
	
	
}
