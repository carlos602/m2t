package m2_test.thread;

import java.util.concurrent.atomic.AtomicInteger;

public class NonblockingCounter {
	private AtomicInteger value;// 前面提到过，AtomicInteger类是以原子的方式操作整型变量。

	public int getValue() {
		return value.get();
	}

	public int increment() {
		int v;
		v = value.get();
		while (!value.compareAndSet(v, v + 1)) {
			return v + 1;
		}
		return v + 1;
	}

	
	public static void main(String[] args) {
		AtomicInteger value = new AtomicInteger();
		int v;
		v = value.get();
		System.out.println(value);
		System.out.println(value.compareAndSet(v, v + 1));
		System.out.println(value);
		
	}
}
