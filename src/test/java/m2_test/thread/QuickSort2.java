package m2_test.thread;

public class QuickSort2 {
	private static boolean isEmpty(int[] n) {
        return n == null || n.length == 0;
    }
    // ///////////////////////////////////////////////////
    /**
     * 快速排序算法思想——挖坑填数方法：
     * 
     * @param n 待排序的数组
     */
    public static void quickSort(int[] n) {
        if (isEmpty(n))
            return;
        quickSort(n, 0, n.length - 1);
    }
    public static void quickSort(int[] n, int l, int h) {
        if (isEmpty(n))
            return;
        if (l < h) {
            int pivot = partion(n, l, h);
            quickSort(n, l, pivot - 1);
            quickSort(n, pivot + 1, h);
        }
    }
    private static int partion(int[] n, int start, int end) {
        int tmp = n[start];
        while (start < end) {
            while (n[end] >= tmp && start < end)
                end--;
            if (start < end) {
                n[start++] = n[end];
            }
            while (n[start] < tmp && start < end)
                start++;
            if (start < end) {
                n[end--] = n[start];
            }
        }
        n[start] = tmp;
        return start;
    }
    
    
    public static void main(String[] args) {
    	int[] a={551 , 603 , 108 , 484 , 719 , 878 , 137 , 888 , 46 , 294 , 101 , 963 , 765 , 29, 413 };  
       
    	System.out.println(a.length);
        for(int i = 0; i < a.length; i++)  
            System.out.print(a[i] + " , ");  
          
        System.out.println("After: ");  
        quickSort(a);  
        for(int i = 0; i < a.length; i++)  
            System.out.print(a[i] + ", ");  
          
        
	}
}
