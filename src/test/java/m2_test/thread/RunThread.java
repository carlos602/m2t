package m2_test.thread;

import java.util.HashMap;
import java.util.Map;

public class RunThread extends Thread{

	
	//volatile 
	private boolean isRuning = true;
//	private volatile boolean isRuning = true;
//	private volatile boolean isRuning = true;
	
	
	public void setRuning(boolean isRuning) {
		this.isRuning = isRuning;
	}
	@Override
	public void run() {
		System.out.println("线程开始。。。");
		while(isRuning){
//			System.out.println(isRuning);
			//..
			try {
				Thread.currentThread().sleep(100);
//				System.out.println("这个是线程：" + Thread.currentThread().getName() + " : isRuning value: " + isRuning);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		System.out.println("线程结束 ");
	}
	
	public static void main(String[] args) throws InterruptedException {

		
		RunThread rt = new RunThread();
		rt.start();
		Thread.sleep(1000);
		
		rt.setRuning(false);
		
		System.out.println(" 设置 runing false");
		
		Thread.sleep(1000);
		
		System.out.println(rt.isRuning);
		
		
		
	
		
	}

}
