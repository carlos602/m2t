package m2_test.thread.local;

/**
 * Created by carlosyang on 16/4/29.
 */
public class ThreadLocalTest {

    public static void main(String[] args) throws InterruptedException {
        ThreadLocal tl = new MyThreadLocal();
        tl.set(new My50MB());
//        My50MB my50MB = new My50MB();
//        my50MB = null;


        System.out.println("Full GC 1");
        System.gc();
        tl.remove();
        tl=null;
        System.out.println("Full GC 2");
        System.gc();
    }

    public static class MyThreadLocal extends ThreadLocal {
        private byte[] a = new byte[1024*1024*1];

        @Override
        public void finalize() {
            System.out.println("My threadlocal 1 MB finalized.");
        }
    }

    public static class My50MB {
        private byte[] a = new byte[1024*1024*50];

        @Override
        public void finalize() {
            System.out.println("My 50 MB finalized.");
        }
    }



}
