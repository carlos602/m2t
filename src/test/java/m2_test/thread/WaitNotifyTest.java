package m2_test.thread;

public class WaitNotifyTest {
	private static final Object lock = new Object();

	public static void main(String[] args) {
		new Thread(new Thread1()).start();
		new Thread(new Thread2()).start();
	}

	static class Thread1 implements Runnable {

		@Override
		public void run() {
			System.out.println("Thread1 start...");
			synchronized (lock) {
				try {
					lock.wait();
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
			System.out.println("Thread1 stop...");
		}
	}

	static class Thread2 implements Runnable {

		@Override
		public void run() {
			System.out.println("Thread2 start...");
			synchronized (lock) {
				lock.notify();
				System.out.println("Thread2 stop...");
			}
		}
	}
}
